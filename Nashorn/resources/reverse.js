/**
 * assumed globals
 * fileIn - the file from which the data is read
 * fileOut - the file to which the data is written
 */

var reader = 
	new java.io.BufferedReader(
			new java.io.FileReader(fileIn));

var reverseWriter = function (w) {
	var writer = 
		new java.io.PrintWriter(
			new java.io.OutputStreamWriter(
				new java.io.FileOutputStream(fileOut)), true);
	return { 
		println: function (input) { writer.println( input.split("").reverse().join("") ); }
	};
}();

(function () {
	for (var line = reader.readLine(); line; line = reader.readLine()) {
		reverseWriter.println(line);
	}
})();