package hello;

import java.io.Reader;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class AccessingGlobalScriptVariable {

	public static void main(String[] args) throws ScriptException {
		ScriptEngine engine = EngineUtil.getEngine();
		Reader script = EngineUtil.getReader("global_year.js");
		engine.eval(script);
		Object year = engine.get("year");
		System.out.println("year's class: " + year.getClass().getName());
		System.out.println("year's value: " + year);
	}

}
