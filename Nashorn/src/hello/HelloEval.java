package hello;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class HelloEval {

	public static void main(String[] args) throws ScriptException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine nashorn = manager.getEngineByName("JavaScript");
		
		evalByFile(nashorn);
	}
	
	static void evalByString(ScriptEngine engine) throws ScriptException {
		String helloScript = "print('Hello Scripting!')";
		engine.eval(helloScript);
	}
	
	static void evalByFile(ScriptEngine engine) throws ScriptException {
		Reader reader =
				new BufferedReader(
						new InputStreamReader(
								HelloEval.class
								.getClassLoader()
								.getResourceAsStream("hello.js")));
		
		engine.eval(reader);
	}

}
