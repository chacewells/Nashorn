package hello;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class SetValueInBinding {

	public static void main(String[] args) throws ScriptException {
		ScriptEngine nashorn = EngineUtil.getEngine();
		Result result = new Result();
		nashorn.put("result", result);
		
		String script = "var happy = 1 + 2; result.setValue(happy * 100);";
		nashorn.eval(script);
		
		System.out.println("The script result value is: " + result.getValue());
	}
	
	public static class Result {
		int value = -1;
		public int getValue() {
			return value;
		}
		public void setValue(int value) {
			this.value = value;
		}
	}
	
}
