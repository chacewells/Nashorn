package hello;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class ReverseScript {

	public static void main(String[] args) throws ScriptException {
		ScriptEngine engine = EngineUtil.getEngine();
		engine.put("fileIn", "hello.txt");
		engine.put("fileOut", "olleh.txt");
		
		engine.eval(EngineUtil.getReader("reverse.js"));
	}

}
