package hello;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class EngineUtil {

	public static synchronized Reader getReader(String filename) {
		return new BufferedReader(
				new InputStreamReader(
						EngineUtil.class
						.getClassLoader()
						.getResourceAsStream(filename)));
	}
	
	public static synchronized ScriptEngine getEngine() {
		return new ScriptEngineManager().getEngineByName("JavaScript");
	}
	
}
