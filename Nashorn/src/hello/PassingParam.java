package hello;

import java.io.IOException;
import java.io.Reader;
import java.util.Date;

import javax.script.ScriptEngine;
import javax.script.ScriptException;


public class PassingParam {

	public static void main(String[] args) throws InterruptedException {
		helloWithParam();
		typeof();
	}
	
	static void helloWithParam() {
		doParam("hello_with_param.js", "msg", "Hello from Java!");
	}
	
	static void typeof() {
		doParam("typeof.js", "param", new Date());
	}
	
	static void doParam(String scriptname, String varname, Object value) {
		try (Reader reader = EngineUtil.getReader(scriptname)) {
			ScriptEngine nashorn = EngineUtil.getEngine();
			nashorn.put(varname, value);
			nashorn.eval(reader);
		} catch (ScriptException|IOException e) {
			e.printStackTrace();
		}
	}

}
