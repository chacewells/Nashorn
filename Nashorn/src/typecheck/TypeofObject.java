package typecheck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class TypeofObject {
	
	public static void main(String[] args) throws ScriptException {
		ScriptEngine nashorn = new ScriptEngineManager().getEngineByName("nashorn");
		nashorn.eval("var obj = {message:'hello'};");
		Object obj = nashorn.get("obj");
		System.out.println(obj.getClass());
		if (obj instanceof Map) {
			System.out.println(((Map<?,?>)obj).get("message"));
		}
		printInterfaces(obj.getClass());;
	}
	
	static void printInterfaces(Class<?> clazz) {
		if (clazz==null) return;
		Collection<Class<?>> interfaces = new ArrayList<>();
		interfaces = Arrays.asList(clazz.getInterfaces());
		System.out.println(clazz != null ? clazz : "");
		if (Object.class.equals(clazz)) {
			return;
		} else {
			printInterfaces(clazz.getSuperclass());
		}
		if (!interfaces.isEmpty()) {
			for (Class<?> iface : clazz.getInterfaces()) {
				printInterfaces(iface);
			}
		}
	}
	
}
